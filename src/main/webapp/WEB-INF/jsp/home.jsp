<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/userListTag.tld"  prefix="h" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<html>
<head>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    HOME
    <h:userList users="${users}"></h:userList>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <h2>Confirmation</h2>
            </div>
            <div class="modal-body">
                <p>Are you sure?</p>
            </div>
            <form method="post" action="delete" name="delete">
                <input type="hidden" name="loginDelete" id="loginDelete" value=""/>
                <button type="submit" name="yes">Yes</button>
                <form action="home.jsp">
                    <input type="submit" value="Cancel" />
                </form>
            </form>
        </div>

    </div>


    <script src="resources/js/javascript.js"></script>
</body>
</html>
