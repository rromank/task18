package reznik.task18.service;

import reznik.task18.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findByLogin(String login);

}
