package reznik.task18.tag;

import reznik.task18.entity.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserListTag extends TagSupport {

    private User user;
    private List<User> users = new ArrayList<>();

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public int doStartTag() throws JspException {
        pageContext.setAttribute("count", users.size());
        try {
            printTableHeader();
            printTableBody();
            printTableFooter();
        } catch (Exception ex) {
            System.err.println("Can't print users: " + ex.getMessage());
        }
        return EVAL_BODY_INCLUDE;
    }

    private void printTableHeader() throws IOException {
        JspWriter out = pageContext.getOut();
        out.print("<table>");
        out.print("<th>Login</th>");
        out.print("<th>First Name</th>");
        out.print("<th>Last Name</th>");
        out.print("<th>Age</th>");
        out.print("<th>Role</th>");
        out.print("<th>Actions</th>");
    }

    private void printTableBody() throws IOException {
        JspWriter out = pageContext.getOut();

        for (User user : users) {
            try {
                out.print("<tr><td>");
                out.print(user.getLogin());
                out.print("</td><td>");
                out.print(user.getFirstName());
                out.print("</td><td>");
                out.print(user.getLastName());
                out.print("</td><td>");
//                out.print(DateUtils.getAge(user.getBirthday()));
                out.print("AGE");
                out.print("</td><td>");
                if (user.getRole() == 1) {
                    out.print("admin");
                } else {
                    out.print("user");
                }
                out.print("</td><td>");
                out.print("<a href=\"crud?login=" + user.getLogin() + "&operation=edit\">Edit</a> ");
                out.print("<input type=\"hidden\" name=\"login\" id=" + user.getLogin() + " />");
                out.print("<a id=\"popup\" href=\"#?" + user.getLogin() + "\" onclick=\"popUp('" + "USER LOGIN" + "');return false;\">Delete</a>");
                out.print("</td>");
                out.print("</tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void printTableFooter() throws IOException {
        JspWriter out = pageContext.getOut();
        out.print("</table>");
    }

}
