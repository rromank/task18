package reznik.task18.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import reznik.task18.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findAll();

    User findByLogin(String login);

    User findByEmail(String email);

}
