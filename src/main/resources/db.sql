DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS user;

CREATE TABLE role(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255)
);

CREATE TABLE user(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	login VARCHAR(20),
	password VARCHAR(24),
	email VARCHAR(40),
	firstname VARCHAR(30),
	lastname VARCHAR(30),
	birthday DATE,
	role_id INT
);

INSERT INTO role (id, name) VALUES (1, 'ADMIN');
INSERT INTO role (id, name) VALUES (2, 'USER');

INSERT INTO user (login, password, email, firstname, lastname, birthday, role_id) VALUES ('rromank', '605747', 'cp0@mail.ru', 'Roman', 'Riznyk', '1992-07-26', 1);
INSERT INTO user (login, password, email, firstname, lastname, birthday, role_id) VALUES ('sasha', '605747', 'sasha@mail.ru', 'Alexandr', 'Riznyk', '1997-10-19', 2);
INSERT INTO user (login, password, email, firstname, lastname, birthday, role_id) VALUES ('petro', '605747', 'petro@mail.ru', 'Petro', 'Petrov', '1995-04-15', 2);

